from constant import COIN_MARKET_CAP_API, get_target_percentage, COIN_NAME, CURRENCY
from requests import Session
import json
import collections
from datetime import datetime
from error_logging import error_log, system_log, initiate_log
from test_email import post_email
import time


class CryptoPrice:
    def __init__(self, price_usd, market_cap, usd_vol, usd_change, last_updated_at):
        self.price_usd = price_usd
        self.market_cap = market_cap
        self.usd_vol = usd_vol
        self.usd_change = usd_change
        self.timestamp = last_updated_at
        self.datetime = datetime.fromtimestamp(self.timestamp)


def fetch_json_to_class(crypto_data):
    this_time = datetime.strptime(crypto_data['last_updated'], '%Y-%m-%dT%H:%M:%S.%fZ')
    this_price = CryptoPrice(crypto_data['price'], crypto_data['market_cap'], crypto_data['volume_24h'],
                             crypto_data['percent_change_24h'], this_time.timestamp())
    system_log("Turned JSON response to class")

    return this_price


def compare_coin_history(coin_history, coin_now):
    history_length = coin_history.__len__()
    counter = 0

    while history_length > 0:
        history_length -= 1
        counter += 1

        system_log("COMPARE IN PROGRESS, counter: " + str(counter))
        target_percentage_change = get_target_percentage(counter)

        if target_percentage_change is not None:
            system_log("\t COMPARABLED: " + str(target_percentage_change))

            # TODO: Bad coding but will change it here
            negative_temp = -1*target_percentage_change

            percentage_change = calculate_percent_change(coin_history[history_length].price_usd, coin_now.price_usd)
            system_log("Old Price: " + str(coin_history[history_length].price_usd) + " New Price:" + str(coin_now.price_usd))
            system_log("Percentage Change " + str(percentage_change) + " Target: " + str(target_percentage_change))
            if percentage_change >= target_percentage_change:

                system_log("\tFOUND PERCENTAGE CHANGE: " + str(percentage_change) + " AT COUNTER: " + str(counter))

                # Send email
                post_email(coin_now.price_usd, coin_history[history_length].price_usd,
                           percentage_change, coin_now.usd_change, target_percentage_change, "GAIN", coin_now.usd_vol
                           , counter)

                return True
            elif percentage_change <= negative_temp:
                system_log("\tFOUND PERCENTAGE CHANGE: " + str(percentage_change) + " AT COUNTER: " + str(counter))

                # Send email
                post_email(coin_now.price_usd, coin_history[history_length].price_usd,
                           percentage_change, coin_now.usd_change, target_percentage_change, "LOSS", coin_now.usd_vol
                           , counter)

                return True
    system_log("COMPARED FULL PRICE HISTORY")


def calculate_percent_change(old_price, new_price):
    return (new_price - old_price)/old_price


# Initialize the collections in deque format for 24 Hours changes
coin_history = collections.deque(maxlen=24)

# Initialize the error system log
initiate_log()

# Configurate the API connection
url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
parameters = {
    'symbol': COIN_NAME,
    'convert': CURRENCY
}
headers = {
    'Accepts': 'application/json',
    'X-CMC_PRO_API_KEY': COIN_MARKET_CAP_API[0],
}

session = Session()
session.headers.update(headers)

system_log("Configurated API connection")

while True:
    response = session.get(url, params=parameters)
    data = json.loads(response.text)

    crypto_info = data['data']['ARGON']['quote']['USD']
    system_log("Fetched Price")

    if response.status_code == 200:
        system_log("Fetch Successfully")

        crypto_price = fetch_json_to_class(crypto_info)

        compare_coin_history(coin_history, crypto_price)

        coin_history.append(crypto_price)
    elif response.status_code == 429:
        system_log("Token exceed limit: " + str(data['status']))
        error_log("API ERROR: TOKEN EXCEEDED LIMIT")
    else:
        error_log("REQUEST ERROR: REQUEST RESPOND CODE: " + response.status_code)

    system_log("")
    time.sleep(300)



