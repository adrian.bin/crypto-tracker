import datetime


def initiate_log():
    error = open("log/error.log", "w")
    error.write("=================STARTING LOG====================\n")
    error.close()

    system = open("log/system.log", "w")
    system.write("================STARTING LOG====================\n")
    system.close()


def error_log(message):
    logf = open("log/error.log", "a")
    logf.write(message + datetime.now() + "\n")
    logf.close()


def system_log(message):
    logf = open("log/system.log", "a")
    logf.write(message + "\n")
    print(message)
    logf.close()