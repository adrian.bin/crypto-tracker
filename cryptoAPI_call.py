import requests
import json
from datetime import datetime
import collections, time
from pycoingecko import CoinGeckoAPI


class Crypto:
    def __init__(self, id, symbol, name, platforms):
        self.id = id
        self.symbol = symbol
        self.name = name
        self.platforms = platforms


class CryptoPrice:
    def __init__(self, price_usd, market_cap, usd_vol, usd_change, last_updated_at):
        self.price_usd = price_usd
        self.market_cap = market_cap
        self.usd_vol = usd_vol
        self.usd_change = usd_change
        self.timestamp = last_updated_at
        self.datetime = datetime.fromtimestamp(self.timestamp)


def get_coin_list():


    url = "http://api.coingecko.com/api/v3/coins/list"

    params = {'include_platform': 'true'}

    response = requests.post(url, params=params)


    coin_list = response.json()

    formulated_coins = []
    for coin in coin_list:
        formulated_coins.append(Crypto(coin['id'], coin['symbol'], coin['name'], coin['platforms']))
        if (coin['symbol'] == 'argon'):
            print(coin['id'], "FOUND");

    print(formulated_coins[0].symbol)

def get_currency_list():
    url = "http://api.coingecko.com/api/v3/simple/supported_vs_currencies"
    response = requests.post(url)

    currency_list = response.json()

    print(currency_list)


def get_coin_price():
    cg = CoinGeckoAPI()

    coin_price = cg.get_price(ids='argon', vs_currencies='usd', include_market_cap='true', include_24hr_vol='true', include_24hr_change='true', include_last_updated_at='true')['argon']
    print(coin_price)

    coin_detail = CryptoPrice(coin_price['usd'], coin_price['usd_market_cap'], coin_price['usd_24h_vol'], coin_price['usd_24h_change'], coin_price['last_updated_at'])

    print(coin_detail.price_usd, coin_detail.datetime)

    return coin_detail



#get_currency_list()
#get_coin_list()
get_coin_price()
coin_history = collections.deque(maxlen=100)
# get_coin_price_bscscan()
# coin_history.append(coin_detail)

# while True:
#     coin_live = get_coin_price()
#     coin_history.append(coin_live)
#     if (coin_live.timestamp != coin_history[0].timestamp):
#         print("Coin has updated: ", coin_live.timestamp, coin_history[0].timestamp)
#         print("Last Updated: ", coin_history[0].datetime)
#         print("Live Time: ", coin_live.datetime)
#         break
#
#     time.sleep(30)
    