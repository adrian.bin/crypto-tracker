EMAIL_SERVER_URL = "http://abc.com"

COIN_NAME = 'ARGON'
CURRENCY = 'USD'

COIN_MARKET_CAP_API = ["ABCDEF"]

PERCENTAGE_ALERT_5MINUTE = 0.1
PERCENTAGE_ALERT_10MINUTE = 0.1
PERCENTAGE_ALERT_15MINUTE = 0.2
PERCENTAGE_ALERT_20MINUTE = None
PERCENTAGE_ALERT_25MINUTE = None
PERCENTAGE_ALERT_30MINUTE = 0.2
PERCENTAGE_ALERT_35MINUTE = None
PERCENTAGE_ALERT_40MINUTE = None
PERCENTAGE_ALERT_45MINUTE = 0.2
PERCENTAGE_ALERT_50MINUTE = None
PERCENTAGE_ALERT_55MINUTE = None
PERCENTAGE_ALERT_1HOUR = 0.2


def get_target_percentage(counter):
    if counter == 1:
        return PERCENTAGE_ALERT_5MINUTE
    elif counter == 2:
        return PERCENTAGE_ALERT_10MINUTE
    elif counter == 3:
        return PERCENTAGE_ALERT_15MINUTE
    elif counter == 4:
        return PERCENTAGE_ALERT_20MINUTE
    elif counter == 5:
        return PERCENTAGE_ALERT_25MINUTE
    elif counter == 6:
        return PERCENTAGE_ALERT_30MINUTE
    elif counter == 7:
        return PERCENTAGE_ALERT_35MINUTE
    elif counter == 8:
        return PERCENTAGE_ALERT_40MINUTE
    elif counter == 9:
        return PERCENTAGE_ALERT_45MINUTE
    elif counter == 10:
        return PERCENTAGE_ALERT_50MINUTE
    elif counter == 11:
        return PERCENTAGE_ALERT_55MINUTE
    elif counter == 12:
        return PERCENTAGE_ALERT_1HOUR
    else:
        return None