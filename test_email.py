import requests
from constant import EMAIL_SERVER_URL, COIN_NAME
from error_logging import error_log


def post_email(price, old_price, percentage_changes, percentage_24h_change, target_alert, alert_type, volume_24h, counter):
    url = EMAIL_SERVER_URL

    parameters = {'coin_name': COIN_NAME, 'price': price, 'old_price': old_price,
                  'percentage_changes': percentage_changes*100, 'percentage_24h_change': percentage_24h_change*100,
                  'target_alert': target_alert*100, 'type': alert_type, 'volume': volume_24h, 'counter': counter*5}

    response = requests.post(url, data=parameters)

    if response.status_code != 200:
        error_log("EMAIL SERVER ERROR:: COULD NOT COMMUNICATE WITH EMAIL SERVER")
    else:
        print("COMMUNICATED WITH EMAIL SERVER")

